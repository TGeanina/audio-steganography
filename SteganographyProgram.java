/**
 * @author Geanina Tambaliuc
 *
 */
import java.awt.List;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Scanner;

public class SteganographyProgram {

	public static void main(String[] args) throws IOException {
		
		/* Ask the user what wants to do */
		Scanner scan = new Scanner(System.in);
		System.out.println("What would you like to do?");
        System.out.println("1 - Insert a file into a wav file");
        System.out.println("2 - Extract a file from a wav file ");
        int command = scan.nextInt();
        

        if (command == 1) {
        try
        {	/*The user wants to encode */
        	System.out.println("Enter the WAV file name that you want the text encoded into (include the .wav extension): ");
            String wavFile=scan.next();
            System.out.println();
            
        	System.out.println("Enter the file name that you want to encode (include the extension) : ");
            String fileToEncode = scan.next();
            System.out.println();
            System.out.println("Enter the name of the new file (include the .wav extension): ");
            String newFile=scan.next();
            encode(fileToEncode,wavFile, newFile);
        }
        catch(Exception e)
        {
        	System.out.println("Exception"+e.getMessage());
        }
        }
        else
        	if(command ==2)
        	{
        		try
        		{/*The user wants to decode*/
            	System.out.println("Enter the WAV file name that you want the text encoded into (include the .wav extension): ");
                String wavFile=scan.next();
                System.out.println();
                
            	System.out.println("Enter the key: ");
                String key=scan.next();
                
                System.out.println("Enter the name of the new file, include the extension (the hidden file should have the same extension): ");
                String newFile=scan.next();
                
                
                System.out.println();
                decode(wavFile,key, newFile);
        		}
        		catch(Exception e)
        		{
        			System.out.println("Exception "+e.getMessage());
        		}
        	}
        	else
        	{
        		/*Wrong input*/
        		System.out.println("Invalid Option!");
        	}
	}

	/**
	 * Decode a wav File
	 * @param wavFile
	 * @param key
	 * @param newFile
	 * @throws IOException
	 */
	private static void decode(String wavFile, String key, String newFile) throws IOException {
		
		/* read the file and store it into a byte array */
		  File file = new File(wavFile); 
		  Path path = Paths.get(file.getAbsolutePath()); 
		  byte[] data = Files.readAllBytes(path);
		  
		  int j=45; int index2=0;
		  
		  /*find the hidden message*/
		  String str="";
		  while(index2<Integer.parseInt(key))
		  {
			  if(data[j]%2==0)
				  str=str+"0";
			  else str=str+"1";
			  
			  j++;
			  index2++;
		  }
		  
		  /*convert the binary string (hidden message) to byte array*/
		  ArrayList<Integer> list = new ArrayList<>();

		  for(String string : str.split("(?<=\\G.{8})"))
		      list.add(Integer.parseInt(string, 2));
		  
		  byte[] hidden =new byte[list.size()];
		  
		  for(int i=0;i<list.size();i++)
			  hidden[i]=list.get(i).byteValue();
		  
		  /*write the new file*/
		  FileOutputStream fos=new FileOutputStream(newFile);
		  fos.write(hidden);
		  fos.close();
		  
		  System.out.println("The file has been decoded!");
		
	}

	/**
	 * Encode into a wav file
	 * @param fileToEncode
	 * @param wavFile
	 * @param newFile
	 * @throws IOException
	 */
	private static void encode(String fileToEncode, String wavFile, String newFile) throws IOException {
		
		
		/*Store the WAV file into byte array*/
		File file = new File(wavFile); 
		Path path = Paths.get(file.getAbsolutePath()); 
		byte[] data = Files.readAllBytes(path);
		
		
		
		/*Store the hidden file into byte array*/
		File file2 = new File(fileToEncode); 
		Path path2 = Paths.get(file2.getAbsolutePath()); 
		byte[] messageByteArr = Files.readAllBytes(path2);
		
		/*Check if the file size is okay*/
		if(messageByteArr.length>data.length-45)
		{
			System.out.println("The file that you want to hide is too big");
		}
		else
		{/* convert the hidden file to binary */
		  StringBuilder binary = new StringBuilder();
		  for (byte b : messageByteArr)
		  {
		     int val = b;
		     for (int i = 0; i < 8; i++)
		     {
		        binary.append((val & 128) == 0 ? 0 : 1);
		        val <<= 1;
		     }
		  }
		
		/*i starts at 45 because the first 44 bytes contain the header*/
		int i=45; int index=0; 
		
		/*loop until the program hides the entire file*/
		while(index<binary.length() && i<data.length)
		  {
			
				//if the bit that we want to hide is 0
			  if(binary.substring(index,index+1).equals("0"))
			  {
				  //check if the byte in the wav file is odd
				  if(data[i]%2==1)
					  data[i]++;
			  }
			  else if(binary.substring(index,index+1).equals("1"))  // the bit that we want to hide is 1
			  {
				 
				//check if the byte in the wav file is even
				  if(data[i]%2==0)
					  {
					  	data[i]++;
					  }
			  }
			  i++;
			  index++;
			  
		  }
		  
		  /*write the new file*/
		  FileOutputStream fos=new FileOutputStream(newFile);
		  fos.write(data);
		  fos.close();
		  
		  System.out.println("The file has been encoded! Your key is: "+binary.length());
		}
		
	}
}
